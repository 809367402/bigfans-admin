import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Router, Route, hashHistory, IndexRedirect} from 'react-router';
import routers from './Routers'
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Router history={hashHistory}>
    {routers}
</Router>, document.getElementById('root'));
registerServiceWorker();
