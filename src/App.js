import React, {Component} from 'react';
import './App.css';
import {Layout} from 'antd';
import SiderCustom from './components/SiderCustom'
import HeaderCustom from './components/HeaderCustom'
const {Footer, Content} = Layout;

class App extends Component {

    state = {
        collapsed: false,
    }
    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed
        })
    }

    render() {
        return (
            <Layout className="ant-layout-has-sider">
                <SiderCustom path={this.props} collapsed={this.state.collapsed}/>
                <Layout>
                    <HeaderCustom toggle={this.toggle}/>
                    <Content className="App-content">
                        {this.props.children}
                    </Content>
                </Layout>
            </Layout>
        );
    }
}

export default App;
