import React from 'react';
import {browserHistory} from 'react-router'
import {Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete , InputNumber} from 'antd'
import SearchInput from '../../../components/SearchInput/SearchInput'
import TagGroup from '../../../components/TagGroup/TagGroup'
import LzEditor from 'react-lz-editor'
import BaseComponent from '../../../components/BaseComponent'
import IntroductionEditor from '../components/ProductIntroductionEditor/ProductIntroductionEditor'
import BrandSearchInput from '../components/BrandSearchInput/BrandSearchInput'
import ProductTags from '../components/ProductTags/ProductTags'
import AppHelper from '../../../AppHelper';
const FormItem = Form.Item;
const {TextArea} = Input

class ProductCreatePage extends BaseComponent {

    constructor(props){
        super(props);
    }

    state = {
        confirmDirty: false,
        autoCompleteResult: [],
        categories : []
    };

    componentDidMount () {
        let url = AppHelper.config.serviceUrl + '/categories'
        fetch(url)
        .then(res => res.json())
        .then((resp) => {
            let categories = AppHelper.tools.formatCategories(resp.data.categories);
            this.setState({categories});
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                const path = `#/app/product/create_step2`;
                browserHistory.push(path);
            }
        });
    }

    receiveHtml = (content) => {
        console.info("content = " + content)
    }

    onIntroductionChange(info){
        console.log("onChange:",info);
    }

    onCategoryChange = (value) => {
        this.props.onCategoryChange(value);
    }

    render() {

        return (
            <div style={this.props.style}>
                <FormItem {...this.formItemLayout} label="商品类别">
                    {this.getFieldDecorator('productGroup.categoryId', {
                        initialValue: [],
                        rules: [{
                            type: 'array',
                            required: true,
                            message: '请选择商品类别!'
                        }],
                    })(
                        <Cascader placeholder="请选择商品类别" 
                                  options={this.state.categories} 
                                  changeOnSelect 
                                  onChange={this.props.onCategoryChange}/>
                    )}
                </FormItem>
                <FormItem {...this.formItemLayout} label="商品名称" hasFeedback>
                    {this.getFieldDecorator('productGroup.name', {
                        rules: [{required: true, message: 'Please input your nickname!', whitespace: true}],
                    })(
                        <Input />
                    )}
                </FormItem>
                <FormItem {...this.formItemLayout} label="产地" hasFeedback>
                    {this.getFieldDecorator('productGroup.origin')(
                        <Input />
                    )}
                </FormItem>
                <FormItem {...this.formItemLayout} label="品牌" hasFeedback>
                    {this.getFieldDecorator('productGroup.brandId')(
                        <BrandSearchInput form={this.props.form}/>
                    )}
                </FormItem>
                <FormItem {...this.formItemLayout} label="标签" hasFeedback>
                    {this.getFieldDecorator('productGroup.tags')(
                        <ProductTags form={this.props.form}/>
                    )}
                </FormItem>
                <FormItem {...this.formItemLayout} label="商品介绍" hasFeedback>
                    {this.getFieldDecorator('productGroup.description', {
                        rules: [{message: '请填写商品介绍!', whitespace: true}],
                    })(
                        <IntroductionEditor form={this.props.form}/>
                    )}
                </FormItem>
            </div>
        );
    }
}

const ProductCreateStep1 = Form.create()(ProductCreatePage);

export default ProductCreateStep1;