import React from 'react';
import {Form, Button, Tabs} from 'antd';
import ProductPanel from '../components/ProductPanel/ProductPanel'
const TabPane = Tabs.TabPane;

class SpecTab extends React.Component {
    constructor(props) {
        super(props);
        this.newTabIndex = 0;
        this.state = {
            activeKey: '1',
            panes: [
                {
                    title: '未保存',
                    key: '1'
                }
            ],
        };
    }

    onChange = (activeKey) => {
        this.setState({activeKey});
    }
    onEdit = (targetKey, action) => {
        this[action](targetKey);
    }
    add = () => {
        this.uuid++;
        const panes = this.state.panes;
        const activeKey = `newTab${this.newTabIndex++}`;
        panes.push({
            title: 'New Tab',
            key: activeKey
        });
        this.setState({panes, activeKey});
    }
    remove = (targetKey) => {
        let activeKey = this.state.activeKey;
        let lastIndex;
        this.state.panes.forEach((pane, i) => {
            if (pane.key === targetKey) {
                lastIndex = i - 1;
            }
        });
        const panes = this.state.panes.filter(pane => pane.key !== targetKey);
        if (lastIndex >= 0 && activeKey === targetKey) {
            activeKey = panes[lastIndex].key;
        }
        this.setState({panes, activeKey});
    }

    render() {
        return (
            <div style={this.props.style}>
                <div style={{marginBottom: 16}}>
                    <Button style={{margin: "0px 0px 20px 0px"}} type="primary" onClick={this.add}>添加商品</Button>
                </div>
                <Tabs
                    hideAdd
                    onChange={this.onChange}
                    activeKey={this.state.activeKey}
                    type="editable-card"
                    onEdit={this.onEdit}
                >
                  {this.state.panes.map((pane , index) => {
                      return (
                          <TabPane tab={pane.title} key={pane.key}>
                            <ProductPanel prodIndex={index} form={this.props.form} specs={this.props.specs}/>
                          </TabPane>
                        )
                      }
                    )
                  }
                </Tabs>
            </div>
        );
    }
}

export default SpecTab;