import React from 'react';
import AttributeWall from '../../../components/AttributeWall/AttributeWall'

class AttributeTab extends React.Component {

	render () {
		return (
				<div style={this.props.style}>
	                <AttributeWall form={this.props.form} attributes={this.props.attributes}/>
	            </div>
			)
	}
}

export default AttributeTab;