import React from 'react';
import {Popconfirm} from 'antd';
import EditableTable from '../../../components/EditableTable/EditableTable';
import SpecEditableCell from './SpecEditableCell'

class SpecEditableTable extends EditableTable {

    constructor(props) {
        super(props);
        let specSize = this.props.specs.length;
        this.fixedColumns = [{
            title: '价格',
            dataIndex: 'price',
            inputType : 'M',
            width: '15%',
            render: (text, record, rowIndex) => this.renderColumns(this.props.specs, rowIndex, specSize + 1, ''),
        }, {
            title: '库存',
            dataIndex: 'stock',
            inputType : 'M',
            width: '15%',
            render: (text, record, rowIndex) => this.renderColumns(this.props.specs, rowIndex, specSize + 2, ''),
        }, {
            title: 'operation',
            dataIndex: 'operation',
            width: '10%',
            render: (text, record, rowIndex) => {
                const editable = true;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                  <a onClick={() => this.editDone(rowIndex, 'save')}>保存</a>
                  <Popconfirm title="Sure to cancel?" onConfirm={() => this.editDone(rowIndex, 'cancel')}>
                    <a>取消</a>
                  </Popconfirm>
                </span>
                                :
                                <span>
                  <a onClick={() => this.edit(rowIndex)}>编辑</a>
                </span>
                        }
                    </div>
                );
            },
        }];

        this.specColumns = [];

        this.columns = this.specColumns.concat(this.fixedColumns);
    }

    getData(){
        let columns = this.props.specs;
        let data = [];
        if(columns){
            let row = {key:0};
            for (var i = 0; i < columns.length; i++) {
                let column = columns[i];
                row[column['id']] = {
                    editable : true,
                    value : ''
                }
            }
            data.push(row);
        }
        return data;
    }

    renderColumns(data, rowIndex, columnIndex, text) {
        const columnDef = this.columns[columnIndex];
        if (columnDef.inputType === 'fixedValue') {
            return text;
        }
        return (<SpecEditableCell
            editable={true}
            value={text}
            form={this.props.form}
            prodIndex={this.props.prodIndex}
            columnDef={columnDef}
            dataIndex={columnDef.dataIndex}
            onChange={value => this.handleChange(columnDef.dataIndex, rowIndex, value)}
        />);
    }
}

export default SpecEditableTable;