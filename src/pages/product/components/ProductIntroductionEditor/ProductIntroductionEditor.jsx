import React from 'react';
import ReactDOM from 'react-dom';
import LzEditor from 'react-lz-editor'

class ProductIntroductionEditor extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			content : ``
		}
	}

	beforeUpload(file){
		console.log("beforeUpload:",file);
	}

	onChange(info){
		console.log("onchange:" + info)
	}

	receiveHtml(content){
		this.props.form.setFieldsValue({'productGroup.description' : content});
	}

	render () {
		const uploadConfig = {
	      QINIU_URL: "http://up.qiniu.com", //上传地址，现在暂只支持七牛上传
	      QINIU_IMG_TOKEN_URL: "http://www.yourServerAddress.mobi/getUptokenOfQiniu.do", //请求图片的token
	      QINIU_PFOP: {
	        url: "http://www.yourServerAddress.mobi/doQiniuPicPersist.do" //七牛持久保存请求地址
	      },
	      QINIU_VIDEO_TOKEN_URL: "http://www.yourServerAddress.mobi/getUptokenOfQiniu.do", //请求媒体资源的token
	      QINIU_FILE_TOKEN_URL: "http://www.yourServerAddress.mobi/getUptokenOfQiniu.do?name=patch", //其他资源的token的获取
	      QINIU_IMG_DOMAIN_URL: "https://image.yourServerAddress.mobi", //图片文件地址的前缀
	      QINIU_DOMAIN_VIDEO_URL: "https://video.yourServerAddress.mobi", //视频文件地址的前缀
	      QINIU_DOMAIN_FILE_URL: "https://static.yourServerAddress.com/", //其他文件地址前缀
	    }

	    //uploadProps 配置方法见 https://ant.design/components/upload-cn/
	    const uploadProps={
	      action: "http://localhost:8081/bigfans-service/api/",
	      onChange: this.onChange,
	      listType: 'picture',
	      fileList: [""],
	      data: (file)=>{//支持自定义保存文件名、扩展名支持
	          console.log("uploadProps data",file)
	        },
	      multiple: true,
	      beforeUpload: this.beforeUpload,
	      showUploadList: true
	    }
		return (
			<LzEditor
		      active={true}
		      importContent={this.state.content}
		      cbReceiver={(content)=>this.receiveHtml(content)}
		      uploadProps={uploadProps}
		      fullScreen={false}
		      convertFormat="html"/>
			)
	}
}

export default ProductIntroductionEditor;