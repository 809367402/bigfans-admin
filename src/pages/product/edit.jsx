import React from 'react';
import {Form, Button , Breadcrumb ,Card , Steps , message , Tabs} from 'antd';
import AppHelper from '../../AppHelper'
import 'whatwg-fetch'
import ProductTab from './edit/ProductTab'
import SpecTab from './edit/SpecTab'
import AttributeTab from './edit/AttributeTab'

const FormItem = Form.Item;
class ProductEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            attributes : [],
            specs:[],
        };
        this.editUrl = AppHelper.config.serviceUrl + '/product';
    }

    handleSubmit = (e) => {
        e.preventDefault();
        // this.props.form.validateFields((err, values) => {
        //     if (!err) {
        //         console.log('Received values of form: ', values);
        //     }
        // });
        let data = {
            productGroup : {},
            attributes : [],
            products:[]
        };
        let values = this.props.form.getFieldsValue();
        data.productGroup = values.productGroup;
        data.productGroup.categoryId = values.productGroup.categoryId.slice(-1).join('');

        let attributes = values.attributes;
        for(let attr in attributes){
            data.attributes.push({
                'optionId' : attr,
                'value' : attributes[attr]
            })
        }

        let products = values.products;
        for (var i = 0; i < products.length; i++) {
            let prod = products[i];
            let formatedSpecs = [];
            let specs = prod.specs;
            for(let id in specs){
                formatedSpecs.push({
                    'optionId' : id,
                    'value' : specs[id]
                })
            }
            prod.specs = formatedSpecs;

            let imgs = prod.imgs;
            let formatedImgs = [];
            for(let index in imgs){
                formatedImgs.push({
                    'path' : imgs[index]
                })
            }
            prod.imgs = formatedImgs;
        }

        data.products = products;

        message.loading('保存中',0);
        fetch(this.createUrl , {
            method : 'POST',
            headers : {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify(data)
        }).then(res => res.json())
        .then((resp) => {
            message.destroy();
            // window.location.hash='/app/products'
        })
        .catch((error) => {
            message.destroy();
        })
    }

    onCategoryChange = (catIds) =>{
        if (catIds.length == 3) {
            let catId = catIds.slice(-1);
            this.fetchSpecs(catId);
            this.fetchAttributes(catId);
        }
    }

    render() {
        return (
            <div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>商品管理</Breadcrumb.Item>
                    <Breadcrumb.Item>编辑商品</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false}>
                    <Form onSubmit={e => {this.handleSubmit(e)}}>
                        <Tabs defaultActiveKey="1" animated={false}>
                            <Tabs.TabPane tab="商品信息" key="1" form={this.props.form}>
                                <ProductTab form={this.props.form}/>
                            </Tabs.TabPane>
                            <Tabs.TabPane tab="规格信息" key="2" form={this.props.form}>
                                <SpecTab form={this.props.form} specs={this.state.specs}/>
                            </Tabs.TabPane>
                            <Tabs.TabPane tab="属性信息" key="3" form={this.props.form}>
                                <AttributeTab form={this.props.form} attributes={this.state.attributes}/>
                            </Tabs.TabPane>
                        </Tabs>
                        <FormItem>
                            <Button type="primary" htmlType="submit" size="large">保存</Button>
                        </FormItem>
                    </Form>
                </Card>
            </div>
        );
    }
}
const ProductEditPage = Form.create()(ProductEdit);
export default ProductEditPage;