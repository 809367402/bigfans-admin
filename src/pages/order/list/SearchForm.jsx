import React from 'react';
import {Table , Button ,Card , Form , Row, Col , Input,Cascader , Select} from 'antd'
import AppHelper from '../../../AppHelper'

class SearchForm extends React.Component {

	state = {
		parentCats : []
	}

	handleBrandChange = () => {

	}

	handleReset = () => {
		this.props.form.resetFields();
	}

	handleSearch = (e) => {
		this.props.changeLoading(true);
		this.props.receiveData([]);
	}

	render () {
		const {getFieldDecorator} = this.props.form;
		const formItemLayout = {
	      labelCol: { span: 6 },
	      wrapperCol: { span: 18 },
	    };
		return (
			<Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
				<Row gutter={24}>
					<Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="订单编号" >
                            {getFieldDecorator('orderNo')(
                                <Input/>
                            )}
                        </Form.Item>
	                </Col>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="收货人手机号" >
		                    {getFieldDecorator('consigneeMobile')(
		                        <Input/>
		                    )}
		                </Form.Item>
	                </Col>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="收货人" >
		                    {getFieldDecorator('consignee')(
		                        <Input/>
		                    )}
		                </Form.Item>
	                </Col>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="支付方式" >
                            {getFieldDecorator('payMethod')(
                                <Select
								    showSearch
								    placeholder="选择支付方式"
								    optionFilterProp="children"
								    allowClear
								    onChange={this.handleBrandChange}
								    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
								>
								    <Select.Option value="alipay">支付宝支付</Select.Option>
								    <Select.Option value="wechat">微信支付</Select.Option>
								    <Select.Option value="online">在线支付</Select.Option>
								</Select>
                            )}
                        </Form.Item>
	                </Col>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="订单状态" >
                            {getFieldDecorator('payMethod')(
                                <Select
								    showSearch
								    placeholder="选择品牌"
								    optionFilterProp="children"
								    allowClear
								    onChange={this.handleBrandChange}
								    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
								>
								    <Select.Option value="unpaid">未支付</Select.Option>
								    <Select.Option value="paid">已支付</Select.Option>
								    <Select.Option value="deliveried">已发货</Select.Option>
								    <Select.Option value="completed">已完成</Select.Option>
								    <Select.Option value="confirmed">已确认</Select.Option>
								    <Select.Option value="canceled">已取消</Select.Option>
								    <Select.Option value="commented">已评价</Select.Option>
								</Select>
                            )}
                        </Form.Item>
	                </Col>
                </Row>
                <Row>
                	<Col span={24} style={{ textAlign: 'right' }}>
			            <Button type="primary" htmlType="submit">Search</Button>
			            <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
			              Clear
			            </Button>
			        </Col>
                </Row>
			</Form>

			)
	}
}

const SpecSearchForm = Form.create()(SearchForm);
export default SpecSearchForm;
