/**
 *
 */
import React from 'react';

class Container extends React.Component {
    render() {
        return (
            <div height="100%">
                {this.props.children}
            </div>
        )

    }
}

export default Container;