import React from 'react';
import {Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete , Switch} from 'antd';
import {Breadcrumb} from 'antd';
import {Card} from 'antd';
import AttrCreateFrom from './create/form'
const FormItem = Form.Item;

class AttrCreatePage extends React.Component {

    render() {
        return (
            <div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>属性管理</Breadcrumb.Item>
                    <Breadcrumb.Item>创建属性</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false}>
                    <AttrCreateFrom/>
                </Card>
            </div>
        );
    }
}

export default AttrCreatePage;