import React from 'react';
import {Table , Button ,Card , Form , Row, Col , Input} from 'antd'
import AppHelper from '../../../AppHelper'

class SearchFrom extends React.Component {

	handleSearch = () => {

	}

	handleReset = () => {
		this.props.form.resetFields();
	}

	render () {
		const {getFieldDecorator} = this.props.form;
		const formItemLayout = {
	      labelCol: { span: 5 },
	      wrapperCol: { span: 19 },
	    };
		return (
			<Form className="ant-advanced-search-form" onSubmit={e => {this.handleSubmit(e)}}>
				<Row gutter={40}>
					<Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="属性名称2" hasFeedback >
		                    {getFieldDecorator('name2')(
		                        <Input/>
		                    )}
		                </Form.Item>
	                </Col>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="属性名称3" >
		                    {getFieldDecorator('name3')(
		                        <Input/>
		                    )}
		                </Form.Item>
	                </Col>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="属性名称4" hasFeedback >
		                    {getFieldDecorator('name4')(
		                        <Input/>
		                    )}
		                </Form.Item>
	                </Col>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="属性名称" hasFeedback >
		                    {getFieldDecorator('name5')(
		                        <Input/>
		                    )}
		                </Form.Item>
	                </Col>
                </Row>
                <Row>
                	<Col span={24} style={{ textAlign: 'right' }}>
			            <Button type="primary" htmlType="submit">Search</Button>
			            <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
			              Clear
			            </Button>
			        </Col>
                </Row>
			</Form>

			)
	}
}

const AttrSearchFrom = Form.create()(SearchFrom);
export default AttrSearchFrom;


