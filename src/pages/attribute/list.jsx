import React from 'react';
import {Table , Breadcrumb ,Card} from 'antd'
import AppHelper from '../../AppHelper'
import AttrSearchForm from './list/searchform'
import 'whatwg-fetch'


const columns = [
  { title: '商品分类', width: 100, dataIndex: 'name', key: 'name'},
  { title: '属性名称', width: 100, dataIndex: 'age', key: 'age'},
  { title: '属性Code', dataIndex: 'address', key: '1', width: 130 },
  { title: '是否为搜索项', dataIndex: 'address', key: '2', width: 130 },
  { title: '输入方式', dataIndex: 'address', key: '3', width: 130 },
  { title: '可选值', dataIndex: 'address', key: '4', width: 130 },
  { title: 'Column 5', dataIndex: 'address', key: '5', width: 130 },
  { title: 'Column 6', dataIndex: 'address', key: '6', width: 130 },
  { title: 'Column 7', dataIndex: 'address', key: '7', width: 130 },
  { title: 'Column 8', dataIndex: 'address', key: '8' },
  {
    title: 'Action',
    key: 'operation',
    fixed: 'right',
    width: 100,
    render: () => <a href="#">action</a>,
  },
];

const data = [];
for (let i = 0; i < 100; i++) {
  data.push({
    key: i,
    name: `Edrward ${i}`,
    age: 32,
    address: `London Park no. ${i}`,
  });
}

class AttributeListPage extends React.Component {

	state = {
	    data: [],
	    pagination: {},
	    loading: false,
	};

	constructor(props) {
		super(props);
	}

	handleTableChange = (pagination, filters, sorter) => {
		this.fetchData({pagination , filters , sorter});
	}

	fetchData = (params = {}) => {
		this.setState({loading:true});
		var self = this;
		let url = AppHelper.config.serviceUrl + '/attributes';
		fetch('' , params).then((response) => {
			setTimeout(function(){
				self.setState({
					loading : false,
					data : response.data
				})
			} , 1000)
			
		})
	}

	componentDidMount(){
		this.fetchData();
	}

	render () {
		return (
			<div id="App-attr-list">
				<Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>属性管理</Breadcrumb.Item>
                    <Breadcrumb.Item>属性列表</Breadcrumb.Item>
                </Breadcrumb>
				<Card bordered={false}>
					<AttrSearchForm></AttrSearchForm>
					<Table columns={columns} 
						   size="middle" 
						   bordered 
						   dataSource={data} 
						   scroll={{x: '130%'}}
						   loading={this.state.loading}
						   onChange={this.handleTableChange} />
				</Card>
			</div>
			)
	}
}

export default AttributeListPage;