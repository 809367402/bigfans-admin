import React from 'react';
import {Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete, Switch,Radio , message} from 'antd';
import {Breadcrumb} from 'antd';
import {Card} from 'antd';
import AppHelper from '../../../AppHelper'
import 'whatwg-fetch'

const FormItem = Form.Item;

/**
 * 属性创建表单
 */
class CreateForm extends React.Component {

    state = {
        inputType : 'S',
        categories : []
    }

    onCategoryChange = () => {
        console.info('category changed');
    }

    onInputTypeChange = (e) => {
        this.setState({inputType : e.target.value})
    }

    componentDidMount () {
        let url = AppHelper.config.catalogServiceUrl + '/categories'
        fetch(url)
        .then(res => res.json())
        .then((resp) => {
            let categories = AppHelper.tools.formatCategories(resp.data);
            console.log(categories)
            this.setState({categories});
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
        //let name = this.props.form.getFieldValue('name');
        //let origin = this.props.form.getFieldValue('origin');
        // 获取表单中数据
        let formVals = this.props.form.getFieldsValue();
        formVals.categoryId = formVals.categoryId.pop();
        let url = AppHelper.config.catalogServiceUrl + '/attributeOption';
        message.loading('保存中',0);
        fetch(url , {
            method : 'POST',
            headers : {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify(formVals)
        }).then(res => res.json())
        .then((resp) => {
            message.destroy();
            window.location.hash='/app/attributes'
        })
        .catch((error) => {
            message.destroy();
        })
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };

        return (
            <Form onSubmit={e => {this.handleSubmit(e)}}>
                <FormItem {...formItemLayout} label="商品类别" >
                    {getFieldDecorator('categoryId', {
                        initialValue: ['1'],
                        rules: [{
                            type: 'array',
                            required: true,
                            message: '请选择商品类别!'
                        }],
                    })(
                        <Cascader placeholder='选择商品类别' options={this.state.categories} onChange={this.onCategoryChange} changeOnSelect/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="属性名称" hasFeedback >
                    {getFieldDecorator('name', {
                        rules: [{required: true, message: '请填写属性名称!', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="是否作为搜索项" hasFeedback>
                    {getFieldDecorator('searchable', {
                        valuePropName: 'checked',
                        initialValue: true,
                    })(
                        <Checkbox/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="是否必填" hasFeedback>
                    {getFieldDecorator('required', {
                        valuePropName: 'checked',
                        initialValue: false,
                    })(
                        <Checkbox/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="排序" hasFeedback>
                    {getFieldDecorator('orderNum', {
                        initialValue: 1
                    })(
                        <Input placeholder="根据排序进行由小到大排列显示" type="number"/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="输入方式" hasFeedback>
                    {getFieldDecorator('inputType' , {
                        initialValue: 'S',
                    })(
                        <Radio.Group onChange={this.onInputTypeChange}>
                            <Radio value='S'>从下面值中选择</Radio>
                            <Radio value='M'>手动输入</Radio>
                        </Radio.Group>
                    )}
                </FormItem>
                {
                    this.state.inputType == 'S'
                    &&
                    <FormItem {...formItemLayout} label="可选值" hasFeedback>
                        {getFieldDecorator('values')(
                            <Select
                                mode="tags"
                                style={{ width: '100%' }}
                                searchPlaceholder="标签模式"
                                placeholder="输入后按回车确认"
                            >
                            </Select>
                        )}
                    </FormItem>
                }
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit" size="large">添加属性</Button>
                </FormItem>
            </Form>
        );
    }

}
const AttrCreateForm = Form.create()(CreateForm);
export default AttrCreateForm;