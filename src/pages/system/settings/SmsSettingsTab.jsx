import React from 'react';
import {Form, Input, Tooltip, Icon, Cascader, Select, Upload, Checkbox, Button, Collapse , Breadcrumb , Tabs , Radio} from 'antd';
const FormItem = Form.Item;
const {TextArea} = Input

class SmsSettingsTab extends React.Component {

    constructor(props){
        super(props);
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };

        return (
            <div>
                <FormItem {...formItemLayout} label="短信平台" hasFeedback >
                    {getFieldDecorator('sms_vendor', {
                        initialValue : 'alidy',
                        rules: [{required: true, message: '请填写access key!', whitespace: true}],
                    })(
                        <Select>
                          <Select.Option value="alidy">阿里大于</Select.Option>
                        </Select>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="app key" hasFeedback >
                    {getFieldDecorator('alidy_appKey', {
                        rules: [{required: true, message: '请填写secret key!', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="secret key" hasFeedback >
                    {getFieldDecorator('alidy_secretKey', {
                        rules: [{required: true, message: '请填写bucket name!', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="sign name" hasFeedback >
                    {getFieldDecorator('alidy_signName', {
                        rules: [{required: true, message: '请填写bucket host name!', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="验证码超时时间" hasFeedback >
                    {getFieldDecorator('sms_timeout', {
                        initialValue : '10',
                        rules: [{required: true, message: '请填写bucket host name!', whitespace: true}],
                    })(
                        <Select>
                          <Select.Option value="10">10分钟</Select.Option>
                          <Select.Option value="20">20分钟</Select.Option>
                          <Select.Option value="30">30分钟</Select.Option>
                          <Select.Option value="60">60分钟</Select.Option>
                        </Select>
                    )}
                </FormItem>
            </div>
        );
    }
}

export default SmsSettingsTab;