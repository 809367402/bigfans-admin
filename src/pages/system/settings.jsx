import React from 'react';
import {Form, Input, Tabs, Card, Cascader, Select, Table, Col, Checkbox, Button, Breadcrumb , message , Radio} from 'antd';
import 'whatwg-fetch'
import AppHelper from '../../AppHelper'
import BasicSettingsTab from './settings/BasicSettingsTab'
import PluginSettingsTab from './settings/PluginSettingsTab'
import EmailSettingsTab from './settings/EmailSettingsTab'
import SmsSettingsTab from './settings/SmsSettingsTab'
import HomeSettingsTab from './settings/HomeSettingsTab'

const FormItem = Form.Item;
const {TextArea} = Input;

class SystemSettings extends React.Component {


    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
        let values = this.props.form.getFieldsValue();
        let url = AppHelper.config.serviceUrl + '/system/updateSettings';
        message.loading('保存中',0);
        fetch(url , {
            method : 'POST',
            headers : {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify(values)
        }).then(res => res.json())
        .then((resp) => {
            message.destroy();
            window.location.hash='/app/system/settings'
        })
        .catch((error) => {
            message.destroy();
        })
        console.info(values);
    }

	render() {
		const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };

		return (
			<div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>系统管理</Breadcrumb.Item>
                    <Breadcrumb.Item>系统设置</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false}>
                    <Form onSubmit={e => {this.handleSubmit(e)}}>
                        <Tabs defaultActiveKey="1" animated={false}>
						    <Tabs.TabPane tab="基本设置" key="1" form={this.props.form}>
						    	<BasicSettingsTab form={this.props.form}/>
						    </Tabs.TabPane>
						    <Tabs.TabPane tab="存储设置" key="2">Content of Tab Pane 3</Tabs.TabPane>
						    <Tabs.TabPane tab="插件设置" key="3">
						    	<PluginSettingsTab form={this.props.form}/>
						    </Tabs.TabPane>
						    <Tabs.TabPane tab="支付设置" key="4">Content of Tab Pane 3</Tabs.TabPane>
                            <Tabs.TabPane tab="邮件设置" key="5">
                                <EmailSettingsTab form={this.props.form}/>
                            </Tabs.TabPane>
                            <Tabs.TabPane tab="短信设置" key="6">
                                <SmsSettingsTab form={this.props.form}/>
                            </Tabs.TabPane>
                            <Tabs.TabPane tab="首页设置" key="home">
                                <HomeSettingsTab form={this.props.form}/>
                            </Tabs.TabPane>
						</Tabs>
                        <FormItem>
                            <Button type="primary" htmlType="submit" size="large">保存</Button>
                        </FormItem>
                    </Form>
                </Card>
            </div>
			)
	}
}
const SystemSettingsPage = Form.create()(SystemSettings);
export default SystemSettingsPage ;
