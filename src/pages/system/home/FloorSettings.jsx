import React from 'react';
import {Form, Input, InputNumber, Checkbox,Select, Button, Card , Breadcrumb , Tabs , Radio} from 'antd';
import AppHelper from '../../../AppHelper';
import 'whatwg-fetch'
const FormItem = Form.Item;
const {TextArea} = Input

class FloorSettingsTab extends React.Component {

    constructor(props){
        super(props);
    }
    state = {
        categories : []
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 2},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };

        return (
            <div>
                <FormItem {...formItemLayout} label="临时图片存储" hasFeedback>
                    {getFieldDecorator('tempImageServer' , {
                        initialValue: 'local',
                    })(
                        <Radio.Group>
                            <Radio value='local'>本地存储</Radio>
                            <Radio value='qiniu'>七牛云服务</Radio>
                        </Radio.Group>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="图片存储" hasFeedback>
                    {getFieldDecorator('imageServer' , {
                        initialValue: 'qiniu',
                    })(
                        <Radio.Group>
                            <Radio value='local'>本地存储</Radio>
                            <Radio value='qiniu'>七牛云服务</Radio>
                        </Radio.Group>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="库存预警" hasFeedback >
                    {getFieldDecorator('stockWarning', {
                        rules: [{required: true, message: '请填写access key!', whitespace: true}],
                    })(
                        <InputNumber style={{width:'auto'}}/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="热门搜索" hasFeedback >
                    {getFieldDecorator('hotSearch', {
                        rules: [{required: true, message: '请填写access key!', whitespace: true}],
                    })(
                        <Select
                            mode="tags"
                            style={{ width: '100%' }}
                            searchPlaceholder="标签模式"
                        >
                        </Select>
                    )}
                </FormItem>
            </div>
        );
    }
}

export default FloorSettingsTab;