import React from 'react';
import {Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete , Switch} from 'antd';
import {Breadcrumb} from 'antd';
import {Card} from 'antd';
import SpecCreateFrom from './create/form'

class SpecCreatePage extends React.Component {

    render() {
        return (
            <div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>规格管理</Breadcrumb.Item>
                    <Breadcrumb.Item>创建规格</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false}>
                    <SpecCreateFrom/>
                </Card>
            </div>
        );
    }
}

export default SpecCreatePage;