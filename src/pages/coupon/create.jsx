import React from 'react';
import {Form, Input, Breadcrumb, Card, InputNumber, Select, Radio, DatePicker ,Col, Checkbox, Button, AutoComplete , Switch} from 'antd';
import BaseComponent from '../../components/BaseComponent'
const { RangePicker } = DatePicker;
const Option = Select.Option;

class CouponCreate extends BaseComponent{

    state = {
        type : 'ER',
        termType : 'FT'
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (err) {
                console.log('Received values of form: ', values);
                return;
            }

            // 获取表单中数据
            let formVals = this.props.form.getFieldsValue();
            let startTime = formVals.startTime.format('YYYY-MM-DD HH:mm:ss');
            let endTime = formVals.endTime.format('YYYY-MM-DD HH:mm:ss');
            formVals.startTime = startTime;
            formVals.endTime = endTime;
            console.info(formVals);
        });
    }

    handleTypeChange = (type) => {
        this.setState({type});
    }

    handleTermTypeChange = (termType) => {
        this.setState({termType});
    }

    render() {
        return (
            <div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>促销管理</Breadcrumb.Item>
                    <Breadcrumb.Item>创建优惠券</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false}>
                    <Form onSubmit={e => {this.handleSubmit(e)}}>
                        <Form.Item {...this.formItemLayout} label="优惠券名称" hasFeedback >
                            {this.getFieldDecorator('name', {
                                rules: [{required: true, message: '请填写优惠券名称!', whitespace: true}],
                            })(
                                <Input/>
                            )}
                        </Form.Item>
                        <Form.Item {...this.formItemLayout} label="满足额度" hasFeedback >
                            {this.getFieldDecorator('condition', {
                                rules: [{required: true, message: '请填写满足额度!'}],
                            })(
                                <InputNumber style={{width:'auto'}}/>
                            )}
                        </Form.Item>
                        <Form.Item {...this.formItemLayout} label="面额" hasFeedback >
                            {this.getFieldDecorator('money', {
                                rules: [{required: true, message: '请填写优惠券名称!'}],
                            })(
                                <InputNumber style={{width:'auto'}} placeholer="可抵扣金额"/>
                            )}
                        </Form.Item>
                        <Form.Item {...this.formItemLayout} label="发放数量" hasFeedback 
                            help="0表示不限量">
                            {this.getFieldDecorator('amount', {
                                rules: [{required: true, message: '请填写发放数量!'}],
                            })(
                                <InputNumber style={{width:'auto'}} placeholer="0表示不限量"/>
                            )}
                        </Form.Item>
                        <Form.Item {...this.formItemLayout} label="发放方式" hasFeedback >
                            {this.getFieldDecorator('distributingType', {
                                initialValue: 'OFFLG',
                                rules: [{required: true, message: '请选择发放方式!'}],
                            })(
                                <Select onChange={this.handleTypeChange}>
                                    <Select.Option value="OG">下单赠送</Select.Option>
                                    <Select.Option value="FG">免费领取</Select.Option>
                                    <Select.Option value="OFFLG">线下发放</Select.Option>
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item {...this.formItemLayout} label="发放期限" hasFeedback>
                            <Col span={5}>
                            <Form.Item>
                                {this.getFieldDecorator('startTime',{
                                    rules: [{required: true}],
                                })(
                                    <DatePicker
                                      showTime
                                      format="YYYY-MM-DD HH:mm:ss"
                                      placeholder="开始日期"
                                    />
                                )}
                            </Form.Item>
                          </Col>
                          <Col span={2}>
                            <p className="ant-form-split">-</p>
                          </Col>
                          <Col span={4}>
                            <Form.Item>
                                {this.getFieldDecorator('endTime',{
                                    rules: [{required: true, message: ''}],
                                })(
                                    <DatePicker
                                      showTime
                                      format="YYYY-MM-DD HH:mm:ss"
                                      placeholder="结束日期"
                                    />
                                )}
                            </Form.Item>
                          </Col>
                        </Form.Item>
                        <Form.Item {...this.formItemLayout} label="使用期限类型" hasFeedback >
                            {this.getFieldDecorator('termType', {
                                initialValue: 'FT',
                                rules: [{required: true}],
                            })(
                                <Select onChange={this.handleTermTypeChange}>
                                    <Select.Option value="FT">固定期间</Select.Option>
                                    <Select.Option value="FD">固定时长(天)</Select.Option>
                                </Select>
                            )}
                        </Form.Item>
                        {
                            this.state.termType === 'FT'
                            &&
                            <Form.Item
                              label="使用期限"
                              {...this.formItemLayout}
                            >
                                <Col span={5}>
                                    <Form.Item>
                                        {this.getFieldDecorator('effectiveTime')(
                                            <DatePicker
                                              showTime
                                              format="YYYY-MM-DD HH:mm:ss"
                                              placeholder="生效日期"
                                            />
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col span={2}>
                                    <p className="ant-form-split">-</p>
                                </Col>
                                <Col span={4}>
                                    <Form.Item>
                                    {this.getFieldDecorator('expiryTime')(
                                        <DatePicker
                                          showTime
                                          format="YYYY-MM-DD HH:mm:ss"
                                          placeholder="失效日期"
                                        />
                                    )}
                                    </Form.Item>
                                </Col>
                            </Form.Item>
                        }
                        {
                            this.state.termType === 'FD'
                            &&
                            <Form.Item {...this.formItemLayout} label="有效天数" hasFeedback >
                                {this.getFieldDecorator('termDays')(
                                    <InputNumber style={{width:'auto'}} placeholer="有效天数"/>
                                )}
                            </Form.Item> 
                        }      
                        <Form.Item {...this.tailFormItemLayout}>
                            <Button type="primary" htmlType="submit" size="large">确认</Button>
                        </Form.Item>
                    </Form>
                </Card>
            </div>
        );
    }
}

export default Form.create()(CouponCreate);
