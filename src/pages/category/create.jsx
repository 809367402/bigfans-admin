import React from 'react';
import {Form, Input, Upload, Card, Cascader, Select, Icon, Col, Checkbox, Button, Breadcrumb , message , Radio} from 'antd';
import AppHelper from '../../AppHelper'
import 'whatwg-fetch'
const FormItem = Form.Item;
const {TextArea} = Input;

class CategoryCreate extends React.Component {

    state = {
        categories : [],
        bordered: true,
        loading: false,
        pagination: false,
        size: 'middle',
        showHeader : true,
        scroll: { y: 240 },
        name : 'abc',
    }

    onParentChange () {
        console.info('parent changed');
    }

    componentDidMount () {
        let url = AppHelper.config.catalogServiceUrl + '/categories'
        fetch(url)
        .then(res => res.json())
        .then((resp) => {
            let categories = AppHelper.tools.formatCategories(resp.data , true);
            this.setState({categories});
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
        //let name = this.props.form.getFieldValue('name');
        //let origin = this.props.form.getFieldValue('origin');
        // 获取表单中数据
        let formVals = this.props.form.getFieldsValue();
        let formParentId = this.props.form.getFieldValue('parentId');
        formVals.level = formParentId.length + 1;
        formVals.parentId = this.props.form.getFieldValue('parentId').pop();
        let url = AppHelper.config.catalogServiceUrl + '/category';
        fetch(url , {
            method : 'POST',
            headers : {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify(formVals)
        }).then(res => res.json())
        .then((resp) => {
            message.destroy();
            window.location.hash='/app/categories'
        })
        .catch((error) => {
            message.destroy();
        })
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };

        return (
            <div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>分类管理</Breadcrumb.Item>
                    <Breadcrumb.Item>创建分类</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false}>
                    <Form onSubmit={e => {this.handleSubmit(e)}}>
                        <FormItem {...formItemLayout} label="上级分类" >
                            {getFieldDecorator('parentId', {
                                initialValue: [],
                                rules: [{
                                    type: 'array',
                                    required: true,
                                    message: '请选择上级分类!'
                                }],
                            })(
                                <Cascader placeholder='选择上级分类' options={this.state.categories} onChange={this.onParentChange} changeOnSelect/>
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="分类名称" hasFeedback >
                            {getFieldDecorator('name', {
                                rules: [{required: true, message: '请填写分类名称!', whitespace: true}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="是否在导航显示" hasFeedback>
                            {getFieldDecorator('showInNav', {
                                valuePropName: 'checked',
                                initialValue: true,
                            })(
                                <Checkbox/>
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="设置为首页推荐" hasFeedback>
                            {getFieldDecorator('showInHome', {
                                valuePropName: 'checked',
                                initialValue: false,
                            })(
                                <Checkbox/>
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="首页推荐大图"
                            extra="显示为楼层主图片"
                        >
                            {getFieldDecorator('upload')(
                                <Upload name="imgPath" 
                                        fileList={this.state.fileList} 
                                        withCredentials={true}
                                        action={this.uploadUrl} 
                                        listType="picture" 
                                        onChange={this.handleLogoChange}
                                        onRemove={this.handleLogoRemove}>
                                    <Button>
                                        <Icon type="upload" />上传
                                    </Button>
                                </Upload>
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="排序" hasFeedback>
                            {getFieldDecorator('orderNum', {
                                initialValue: 1
                            })(
                                <Input placeholder="根据排序进行由小到大排列显示" type="number"/>
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="类型描述" hasFeedback>
                            {getFieldDecorator('description')(
                                <TextArea rows={4} placeholder="类型描述信息"/>
                            )}
                        </FormItem>
                        {
                           /*
                        <FormItem {...formItemLayout} label="规格列表" >
                            <Table columns={columns} dataSource={data} pagination={false} scroll={{ y: 240 }} />
                        </FormItem>
                        <FormItem {...formItemLayout} label="属性列表" >
                            <Table columns={columns} dataSource={data} pagination={false} scroll={{ y: 240 }} />
                        </FormItem>
                        */ 
                        }
                        <FormItem {...tailFormItemLayout}>
                            <Button type="primary" htmlType="submit" size="large">创建分类</Button>
                        </FormItem>
                    </Form>
                </Card>
            </div>
        );
    }
}

const CategoryCreatePage = Form.create()(CategoryCreate);

export default CategoryCreatePage;