/**
 * Created by lichong on 7/14/2017.
 */
import React from 'react';
import App from './App';
import Container from './pages/Container';
import {Route, IndexRedirect} from 'react-router';
import CategoryCreatePage from './pages/category/create';
import CategoryListPage from './pages/category/list';
import AttrCreatePage from './pages/attribute/create';
import AttrListPage from './pages/attribute/list';
import SpecCreatePage from './pages/specification/create';
import SpecListPage from './pages/specification/list'
import BrandCreatePage from './pages/brand/create';
import BrandEditPage from './pages/brand/edit';
import BrandListPage from './pages/brand/list';
import ProductListPage from './pages/product/list';
import ProductCreatePage from './pages/product/create';
import ProductEditPage from './pages/product/edit';
import OrderListPage from './pages/order/list';
import SystemSettingPage from './pages/system/settings';
import ProductPromotionCreatePage from './pages/promotion/product/create';
import OrderPromotionCreatePage from './pages/promotion/order/create';
import CouponCreatePage from './pages/coupon/create';
import CouponListPage from './pages/coupon/list';
import SeckillCreatePage from './pages/seckill/create';
import UserListPage from './pages/user/list'
import StatisticsPage from './pages/statistics'
import DashboardPage from './pages/dashboard'
import './index.css';

const routers =
    <Route path={'/app'} components={App}>
        <IndexRedirect to="/app/dashboard"/>
        <Route path={'dashboard'} component={DashboardPage}/>
        <Route path={'products'} component={ProductListPage}/>
        <Route path={'product'}>
            <Route path={'create'} component={ProductCreatePage}/>
            <Route path={':id'} component={ProductEditPage}/>
        </Route>
        <Route path={'orders'} component={OrderListPage}/>
        <Route path={'categories'} component={CategoryListPage}/>
        <Route path={'category'}>
            <Route path={'create'} component={CategoryCreatePage}/>
        </Route>
        <Route path={'attributes'} component={AttrListPage}/>
        <Route path={'attribute'}>
            <Route path={'create'} component={AttrCreatePage}/>
        </Route>
        <Route path={'specs'} component={SpecListPage}/>
        <Route path={'spec'}>
            <Route path={'create'} component={SpecCreatePage}/>
        </Route>
        <Route path={'brands'} component={BrandListPage}/>
        <Route path={'brand'}>
            <Route path={'create'} component={BrandCreatePage}/>
            <Route path={':id'} component={BrandEditPage}/>
        </Route>
        <Route path={'promotion'}>
            <Route path={'product'} component={ProductPromotionCreatePage}/>
            <Route path={'order'} component={OrderPromotionCreatePage}/>
            <Route path={'seckill'} component={SeckillCreatePage}/>
        </Route>
        <Route path={'coupons'} component={CouponListPage}/>
        <Route path={'coupon'}>
            <Route path={'create'} component={CouponCreatePage}/>
        </Route>
        <Route path={'users'} component={UserListPage}/>
        <Route path={'system'}>
            <Route path={'settings'} component={SystemSettingPage}/>
        </Route>
        <Route path={'login'} components={App}/>
        <Route path={'statistics'} components={StatisticsPage}/>
        <Route path={'404'} component={App}/>
    </Route>

export default routers;