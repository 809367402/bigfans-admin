import React, { Component } from 'react';
import { Layout, Menu, Icon } from 'antd';
import { Link } from 'react-router';
const { Sider } = Layout;
const SubMenu = Menu.SubMenu;

class SiderCustom extends Component {
    state = {
        collapsed: false,
        mode: 'inline',
        openKey: '',
        selectedKey: ''
    };
    componentDidMount() {

    }
    componentWillReceiveProps(nextProps) {
        this.onCollapse(nextProps.collapsed);
    }
    onCollapse = (collapsed) => {
        this.setState({
            collapsed,
            mode: collapsed ? 'vertical' : 'inline',
        });
    };
    menuClick = e => {
        this.setState({
            selectedKey: e.key
        });
    };
    openMenu = v => {
        this.setState({
            openKey: v[v.length - 1]
        })
    };
    render() {
        return (
            <Sider
                trigger={null}
                breakpoint="lg"
                collapsed={this.props.collapsed}
                style={{overflowY: 'auto'}}
            >
                <div className="App-logo" />
                <Menu
                    onClick={this.menuClick}
                    theme="dark"
                    mode={this.state.mode}
                    selectedKeys={[this.state.selectedKey]}
                    openKeys={[this.state.openKey]}
                    onOpenChange={this.openMenu}
                >
                    <Menu.Item key="/app/dashboard">
                        <Link to={'/app/dashboard'}><Icon type="mobile" /><span className="nav-text">首页</span></Link>
                    </Menu.Item>
                    <SubMenu key="/app/product" title={<span><Icon type="scan" /><span className="nav-text">商品管理</span></span>}>
                        <Menu.Item key="/app/product/create"><Link to={'/app/product/create'}>创建商品</Link> </Menu.Item>
                        <Menu.Item key="/app/products"><Link to={'/app/products'}>商品列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/app/order" title={<span><Icon type="scan" /><span className="nav-text">订单管理</span></span>}>
                        <Menu.Item key="/app/orders"><Link to={'/app/orders'}>订单列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/app/category" title={<span><Icon type="scan" /><span className="nav-text">分类管理</span></span>}>
                        <Menu.Item key="/app/category/create"><Link to={'/app/category/create'}>创建分类</Link> </Menu.Item>
                        <Menu.Item key="/app/categories"><Link to={'/app/categories'}>分类列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/app/attribute" title={<span><Icon type="scan" /><span className="nav-text">属性管理</span></span>}>
                        <Menu.Item key="/app/attribute/create"><Link to={'/app/attribute/create'}>创建属性</Link> </Menu.Item>
                        <Menu.Item key="/app/attributes"><Link to={'/app/attributes'}>属性列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/app/spec" title={<span><Icon type="scan" /><span className="nav-text">规格管理</span></span>}>
                        <Menu.Item key="/app/spec/create"><Link to={'/app/spec/create'}>创建规格</Link> </Menu.Item>
                        <Menu.Item key="/app/specs"><Link to={'/app/specs'}>规格列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/app/brand" title={<span><Icon type="scan" /><span className="nav-text">品牌管理</span></span>}>
                        <Menu.Item key="/app/brand/create"><Link to={'/app/brand/create'}>创建品牌</Link> </Menu.Item>
                        <Menu.Item key="/app/brands"><Link to={'/app/brands'}>品牌列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/app/promotion" title={<span><Icon type="scan" /><span className="nav-text">促销管理</span></span>}>
                        <Menu.Item key="/app/promotion/product"><Link to={'/app/promotion/product'}>商品促销</Link> </Menu.Item>
                        <Menu.Item key="/app/promotion/order"><Link to={'/app/promotion/order'}>订单促销</Link> </Menu.Item>
                        <Menu.Item key="/app/promotion/group"><Link to={'/app/promotion/group'}>团购管理</Link> </Menu.Item>
                        <Menu.Item key="/app/seckill"><Link to={'/app/promotion/seckill'}>秒杀管理</Link> </Menu.Item>
                        <Menu.Item key="/app/coupon"><Link to={'/app/coupons'}>优惠券管理</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu key="/app/user" title={<span><Icon type="scan" /><span className="nav-text">用户管理</span></span>}>
                        <Menu.Item key="/app/users"><Link to={'/app/users'}>用户列表</Link> </Menu.Item>
                    </SubMenu>
                    <SubMenu
                        key="system"
                        title={<span><Icon type="switcher" /><span className="nav-text">系统</span></span>}
                    >
                        <Menu.Item key="/app/system/settings"><Link to={'/app/system/settings'}>系统配置</Link></Menu.Item>
                        <Menu.Item key="/app/system/home"><Link to={'/app/system/home'}>首页配置</Link></Menu.Item>
                    </SubMenu>
                    <SubMenu
                        key="statistics"
                        title={<span><Icon type="switcher" /><span className="nav-text">统计</span></span>}
                    >
                        <Menu.Item key="/app/statistics"><Link to={'/app/statistics'}>统计</Link></Menu.Item>
                    </SubMenu>
                    <SubMenu
                        key="pages"
                        title={<span><Icon type="switcher" /><span className="nav-text">页面</span></span>}
                    >
                        <Menu.Item key="/login"><Link to={'/login'}>登录</Link></Menu.Item>
                        <Menu.Item key="/404"><Link to={'/404'}>404</Link></Menu.Item>
                    </SubMenu>
                </Menu>
            </Sider>
        )
    }
}

export default SiderCustom;