import React from 'react';

class BaseComponent extends React.Component {

    constructor(props) {
        super(props);
        if (this.props.form) {
            this.getFieldDecorator = this.props.form.getFieldDecorator;
            this.formItemLayout = {
                labelCol: {
                    xs: {span: 20},
                    sm: {span: 3},
                },
                wrapperCol: {
                    xs: {span: 24},
                    sm: {span: 14},
                },
            };
            this.tailFormItemLayout = {
                wrapperCol: {
                    xs: {
                        span: 24,
                        offset: 0,
                    },
                    sm: {
                        span: 14,
                        offset: 6,
                    },
                },
            };
        }
    }
}

export default BaseComponent;