/**
 * Created by lichong on 2017/7/15.
 */

var koa = require('koa');
var app = new koa();
var router = require('koa-router')();
var koaBody = require('koa-body')();



router.get("/category/list" , function *(next) {
    this.body = {
        "data" : [{
            "name" : "1"
        }]
    };
})

app.use(router.routes()).use(router.allowedMethods());
app.listen(3000);

